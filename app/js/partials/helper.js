/**
 * By Nikolay Kovalenko 23.05
 */



/**
 * Socials 
 */

$("#share").jsSocials({
    showLabel: false,
    showCount: false,
    shares: ["twitter", "facebook", "googleplus", "linkedin"]
});


/**
 * Bottom fixed Banner
 */

function checkOffset() {
    if ($('.bottom-fixed-banner').offset().top + $('.bottom-fixed-banner').height() >= $('.footer').offset().top - 10)
        $('.bottom-fixed-banner').css({
            'position': 'absolute',
            'top': '-70px'
        });
    if (window.matchMedia('(max-width: 745px)').matches) {
        $('.bottom-fixed-banner').css({
            'position': 'absolute',
            'top': '-95px'
        });
    }
    if ($(document).scrollTop() + window.innerHeight < $('.footer').offset().top)
        $('.bottom-fixed-banner').css({
            'position': 'fixed',
            'top': 'auto'
        });
}
$(document).scroll(function() {
    if ($('.bottom-fixed-banner').length > 0) {
        checkOffset();
    }
});

checkOffset();

$('.bottom-fixed-banner__close').click(function() {
    $('.bottom-fixed-banner').fadeOut();
});



/**
 * Popaps 
 */

$(document).ready(function() {
    $('.popup-with-zoom-anim').magnificPopup({
        type: 'inline',

        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
    });

    $('.exit-popup-open').magnificPopup({
        type: 'inline',

        fixedContentPos: false,
        fixedBgPos: true,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
    });

    $(document).on('click', '.popup-modal-dismiss', function(e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    $('#exit-popup .mfp-close').remove();
});