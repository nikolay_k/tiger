/*
 Third party
 */
//= ../../bower_components/jquery/dist/jquery.min.js

/*
    Custom
 */
//= partials/bootstrap.min.js
//= partials/jssocials.min.js
//= partials/classie.js
//= partials/jquery.cookie.min.js
//= partials/jquery.magnific-popup.min.js
//= partials/helper.js